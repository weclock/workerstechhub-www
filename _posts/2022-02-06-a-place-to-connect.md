---
layout: post
title:  "A place to connect...."
date:   2022-02-06 15:32:14 -0300
categories: news
---

You've found our new place to connect, share and learn. More coming soon...

For now, you can check out our ["Cafe" Discussion Boards](https://cafe.workerstechhub.org) and our live [Convene Chat Room](https://workerstechhub.org/chat/#/room/%23workerstechhub:neo.keanu.im)
